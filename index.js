// Using DOM &nbsp;
// Retrieve an element from webpage
const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector(`#txt-last-name`);
let spanFirstName = document.querySelector('#span-first-name');
let spanLastName = document.querySelector(`#span-last-name`)

/* Alternatie in retrieving an element - getElement
	document.getElementById('txt-first-name');
	document.getElementsByClassName('txt-inputs');
	document.getElementsByTagName('input');
*/

function changeValue (elementToChange, inputElement) {
	elementToChange.innerHTML = inputElement.value;
}

// Event listener - an interaction between the user and the web page.
txtFirstName.addEventListener('keyup', (e) => {
	changeValue(spanFirstName, txtFirstName);
});

txtLastName.addEventListener('keyup', (e) => {
	changeValue(spanLastName, txtLastName);
});

// Assign same event to multiple listeners
txtFirstName.addEventListener('keyup', (e) => {

	// Contains the element to multiple listeners
	console.log(event.target);

	// Gets the value of the input object
	console.log(event.target.value);
});
